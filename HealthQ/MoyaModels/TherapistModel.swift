//
//  TherapistModel.swift
//  HealthQ
//
//  Created by Zaini on 30/04/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import Foundation

// All Therapists
typealias TherapistListingResult = [TherapistListingResultElement]?

struct TherapistListingResultElement: Codable {
    let userID: Int?
    let firstName: String?
    let middleName: String?
    let lastName: String?
    let signature, profile, picture: String?
    let clinicID: Int?
    let clinicName: String?
    let latitude, longitude: Double?
    let address: String?
    let city, state: String?
    let zipCode: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "USER_ID"
        case firstName = "FIRST_NAME"
        case middleName = "MIDDLE_NAME"
        case lastName = "LAST_NAME"
        case signature = "Signature"
        case profile = "Profile"
        case picture = "Picture"
        case clinicID = "CLINIC_ID"
        case clinicName = "CLINIC_NAME"
        case latitude = "LATITUDE"
        case longitude = "LONGITUDE"
        case address = "ADDRESS"
        case city = "CITY"
        case state = "STATE"
        case zipCode = "ZIP_CODE"
    }
}


// Theapist Location and Therapist by ID
typealias TherapistLocationResult = [TherapistLocationResultElement]?

struct TherapistLocationResultElement: Codable {
    let userID: Int?
    let firstName: String?
    let middleName: String?
    let lastName: String?
    let signature, profile, picture: String?
    let clinicName: String?
    let latitude, longitude: Double?
    let city: String?
    let address: String?
    
    enum CodingKeys: String, CodingKey {
        case userID = "USER_ID"
        case firstName = "FIRST_NAME"
        case middleName = "MIDDLE_NAME"
        case lastName = "LAST_NAME"
        case signature = "Signature"
        case profile = "Profile"
        case picture = "Picture"
        case clinicName = "CLINIC_NAME"
        case latitude = "LATITUDE"
        case longitude = "LONGITUDE"
        case city = "CITY"
        case address = "ADDRESS"
    }
}


// Therapist Detail
struct TherapistDetailResult: Codable {
    let schedules: [Schedule]?
    let userID: Int?
    let firstName: String?
    let middleName: String?
    let lastName, signature: String?
    let picture, profile: String?
    let clinicID: Int?
    let clinicName: String?
    let latitude, longitude: Double?
    let address, city, state, zipCode: String?
    let companyID: Int?
    
    enum CodingKeys: String, CodingKey {
        case schedules
        case userID = "USER_ID"
        case firstName = "FIRST_NAME"
        case middleName = "MIDDLE_NAME"
        case lastName = "LAST_NAME"
        case signature = "Signature"
        case picture = "Picture"
        case profile = "Profile"
        case clinicID = "CLINIC_ID"
        case clinicName = "CLINIC_NAME"
        case latitude = "LATITUDE"
        case longitude = "LONGITUDE"
        case address = "ADDRESS"
        case city = "CITY"
        case state = "STATE"
        case zipCode = "ZIP_CODE"
        case companyID = "COMPANY_ID"
    }
}

struct Schedule: Codable {
    let scheduleID: Int?
    let scheduleName: String?
    let weekDayID: Int?
    let startTime, endTime: String?
    
    enum CodingKeys: String, CodingKey {
        case scheduleID = "SCHEDULE_ID"
        case scheduleName = "SCHEDULE_NAME"
        case weekDayID = "WEEK_DAY_ID"
        case startTime = "START_TIME"
        case endTime = "END_TIME"
    }
}

// Therapist Schedule

typealias TherapistScheduleResult = [TherapistScheduleResultElement]?

struct TherapistScheduleResultElement: Codable {
    let scheduleName, startDate: String?
    let endDate: String?
    let week1, week2, week3, week4: Bool?
    let week5: Bool?
    let startTime, endTime: String?
    let weekDayID: Int?
    
    enum CodingKeys: String, CodingKey {
        case scheduleName = "SCHEDULE_NAME"
        case startDate = "START_DATE"
        case endDate = "END_DATE"
        case week1 = "Week1"
        case week2 = "Week2"
        case week3 = "Week3"
        case week4 = "Week4"
        case week5 = "Week5"
        case startTime = "START_TIME"
        case endTime = "END_TIME"
        case weekDayID = "WEEK_DAY_ID"
    }
}

struct LoginResult: Codable {
    let patientMrn: String?
    
    enum CodingKeys: String, CodingKey {
        case patientMrn = "PATIENT_MRN"
    }
}

struct AppointmentResult: Codable {
    let response, patientMrn, appointmentDatetime: String?
    
    enum CodingKeys: String, CodingKey {
        case response = "RESPONSE"
        case patientMrn = "PATIENT_MRN"
        case appointmentDatetime = "APPOINTMENT_DATETIME"
    }
}

struct GetTherapistResult: Codable {
    let totalTherapist: Int?
    
    enum CodingKeys: String, CodingKey {
        case totalTherapist = "total_therapist"
    }
}
