//
//  ProfileController.swift
//  HealthQ
//
//  Created by Zaini on 01/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit

class ProfileController: BaseController {

    @IBOutlet weak var PersonName:UILabel!
    @IBOutlet weak var PersonIntro:UITextView!
    @IBOutlet weak var PersonWorking:UILabel!
    @IBOutlet weak var TherapyWorking:UILabel!
    @IBOutlet weak var ContinueBtn:UIButton!
    @IBOutlet weak var detailHeight:NSLayoutConstraint!
    var clinicID = Int()
    var id = Int()
    var weekName = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
          self.navigationItem.title = "Therapist Profile"
          ContinueBtn.roundView(with: 5)
        [PersonName,PersonIntro,PersonWorking,TherapyWorking].forEach
            {$0?.showAnimatedSkeleton()}
          GetProfileDetail()
    }
    
    @IBAction func ContinueBtns(sender:UIButton){
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "HorizontalCalendarController") as? HorizontalCalendarController{
           vc.clinicID = clinicID
           vc.therapistID = id
           vc.weekName = weekName
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
           self.navigationController?.pushViewController(vc, animated: true)
           // loading.startAnimating()
          //  self.view.isUserInteractionEnabled = false
           // ContinueBtn.showSkeleton(usingColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
          //  ShowResults(Message: "HY,How are you")
        }
    }

    private func GetProfileDetail(){
        MoyaNetworkCallGenericFunction(TherapistService.GetTherapistById(Id: id),
                                       { (error) in
                                        self.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                                        
        }) {[weak self] (response) in
            guard let weak = self else {return}
            do{
                let json = try JSONDecoder().decode(TherapistDetailResult.self, from: response.data)
                weak.PersonIntro.text = json.profile ?? "No details given"
                let difference = (json.profile?.heightWithConstrainedWidth(width: 374, font: UIFont(name: "Avenir", size: 16)!))
                if Int(difference ?? 0) == 0{
                    weak.detailHeight.constant = 50
                }else if Int(difference ?? 0) > 0 && Int(difference ?? 0) <= 200{
                    weak.detailHeight.constant = (difference ?? 0) + 50
                }
                    weak.PersonName.text = "\(json.firstName ?? "") \(json.lastName ?? "")"
                    weak.TherapyWorking.text = "\(json.clinicName ?? "") \(json.address ?? "")"
                    weak.clinicID = json.clinicID ?? 0
                if json.schedules?.count != 0 {
                    
                    if let schedule = json.schedules{
                        for week in schedule{
                            if week.weekDayID == 1{
                                weak.PersonWorking.text = "Mon "
                                weak.weekName.append("Mon")
                            }
                            if week.weekDayID == 2{
                                weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "Tue "
                                weak.weekName.append("Tue")
                            }
                            if week.weekDayID == 3{
                                weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "Wed "
                                weak.weekName.append("Wed")
                            }
                            if week.weekDayID == 4{
                                weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "Thu "
                                weak.weekName.append("Thu")
                            }
                            if week.weekDayID == 5{
                                weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "Fri "
                                weak.weekName.append("Fri")
                            }
                            if week.weekDayID == 6{
                                weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "Sat "
                                weak.weekName.append("Sat")
                            }
                            if week.weekDayID == 7{
                                weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "Sun "
                                weak.weekName.append("Sun")
                            }
                        }
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "HH:mm:ss"
                        var date12 = dateFormatter.date(from: json.schedules?[0].startTime ?? "")
                        dateFormatter.dateFormat = "h:mm a"
                        var date22 = dateFormatter.string(from: date12 ?? Date())
                        weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + "(\(date22)"
                        dateFormatter.dateFormat = "HH:mm:ss"
                        date12 = dateFormatter.date(from: json.schedules?[0].endTime ?? "")
                        dateFormatter.dateFormat = "h:mm a"
                        date22 = dateFormatter.string(from: date12 ?? Date())
                        weak.PersonWorking.text = (weak.PersonWorking.text ?? "") + " TO \(date22))"
                        [weak.PersonName,weak.PersonWorking,weak.PersonIntro,weak.TherapyWorking].forEach
                            {$0?.hideSkeleton()}
                    }
                }
                else{
                    weak.PersonIntro.text = "No details given"
                    weak.PersonWorking.frame.size.height = 21
                    weak.PersonWorking.text = "No working hours given"
                    weak.ShowResults(Message: "No working hours given", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                [weak.PersonName,weak.PersonWorking,weak.PersonIntro,weak.TherapyWorking].forEach
                        {$0?.hideSkeleton()}
                }
            }
            catch{
                [weak.PersonName,weak.PersonWorking,weak.PersonIntro,weak.TherapyWorking].forEach
                    {$0?.hideSkeleton()}
                weak.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
            }
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
