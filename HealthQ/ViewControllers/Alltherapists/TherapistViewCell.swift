//
//  TherapistViewCell.swift
//  HealthQ
//
//  Created by Zaini on 01/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit

class TherapistViewCell: UITableViewCell {

    @IBOutlet weak var PersonName:UILabel!
    @IBOutlet weak var PersonPhoto:UIImageView!
    @IBOutlet weak var PersonStatus:UILabel!
    @IBOutlet weak var cardView:UIView!
    @IBOutlet weak var ProfileView:UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        cardView.setViewCard()
        ProfileView.roundView(with: ProfileView.layer.frame.height/2, #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1), 1, true)
        PersonPhoto.roundView(with: PersonPhoto.frame.height/2)
        [PersonName,PersonStatus].forEach
            {$0?.showAnimatedSkeleton()}
        PersonPhoto.showAnimatedSkeleton()
    }
    
    func hideAnimation(){
        [PersonName,PersonStatus].forEach
            {$0?.hideSkeleton()}
        PersonPhoto.hideSkeleton()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
