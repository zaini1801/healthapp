//
//  DiscoverTherapyController.swift
//  HealthQ
//
//  Created by Zaini on 30/04/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Kingfisher
import SkeletonView

struct PersonInfo {
    var name = String()
    var expert = String()
    var Photo = String()
    var id = Int()
    var latlng = CLLocationCoordinate2D()
}
struct PersonLocation {
    var fullName = String()
    var address = String()
    var clinic = String()
    var latlng = CLLocationCoordinate2D()
}
class DiscoverTherapyController: BaseController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate,CLLocationManagerDelegate,MKMapViewDelegate {

    var PersonData = [PersonInfo]()
    var filteredPerson = [PersonInfo]()
    var personLocation = [PersonLocation]()
    @IBOutlet weak var tableView:UITableView!
    @IBOutlet weak var allBtn:UIButton!
    @IBOutlet weak var nearBtn:UIButton!
    @IBOutlet weak var searchField:UITextField!
    @IBOutlet weak var searchView:UIView!
    @IBOutlet weak var mapView:MKMapView!
    @IBOutlet weak var refreshBtn:UIButton!
    
    var refresh = UIRefreshControl()
    let manager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Discover Therapist"
       // mapView.showAnimatedSkeleton()
        searchView.setViewCard()
        mapView.roundView(with: 5, #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0), 1, true)
        tableView.refreshControl = refresh
        tableView.addSubview(refresh)
        refresh.addTarget(self, action: #selector(RefeshMe), for: .valueChanged)
        manager.delegate = self
        mapView.showsUserLocation = true
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
        getTheapistsData()
        getTheapistsLocation()
        mapView.delegate = self
        mapView.addSubview(refreshBtn)
    }
    
    @objc func RefeshMe(sender:Any?){
        getTheapistsData()
      
    }
    
    @IBAction func RefeshBTN(sender:UIButton){
        getTheapistsLocation()
    }
    
    var location = CLLocationCoordinate2D()
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]){
        if let loc = locations.last?.coordinate{
            location = loc
            let span : MKCoordinateSpan = MKCoordinateSpan(latitudeDelta: 21.0, longitudeDelta: 21.0)
            let region : MKCoordinateRegion = MKCoordinateRegion(center: loc, span: span)
            mapView.setRegion(region, animated: true)
            manager.stopUpdatingLocation()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if isFiltering() {
            return filteredPerson.count
        }
        return PersonData.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? TherapistViewCell else{return UITableViewCell()}
        let person: PersonInfo
        if isFiltering() {
            person = filteredPerson[indexPath.row]
        } else {
            person = PersonData[indexPath.row]
        }
        cell.hideAnimation()
        cell.PersonName.text = person.name
        cell.PersonPhoto.kf.setImage(with: URL(string: Image_URL + person.Photo), placeholder: #imageLiteral(resourceName: "doctor"))
        cell.PersonStatus.text = person.expert
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileController") as? ProfileController{
            if isFiltering(){
                vc.id = filteredPerson[indexPath.row].id
            }else{
                vc.id = PersonData[indexPath.row].id
            }
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func AllNearBtn(sender:UIButton){
        if sender.tag == 0{
            allBtn.layer.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            nearBtn.layer.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.7529411765, blue: 0.9529411765, alpha: 1)
            tableView.isHidden = false
            mapView.isHidden = true
        }else{
            nearBtn.layer.backgroundColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
            allBtn.layer.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.7529411765, blue: 0.9529411765, alpha: 1)
            tableView.isHidden = true
            mapView.isHidden = false
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView)
    {
        if let annotation = view.annotation
        {
    
            if annotation.title != "My Location"{
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "ProfileController") as? ProfileController{
                    let id:String? = annotation.subtitle ?? ""
                    vc.id = Int(id ?? "") ?? 0
                    navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
                    navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
        }
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        print("deselected")
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        print("String: " + string)
        if string != ""{
            filterContentForSearchText(textField.text ?? "" + string)
        }else{
            filterContentForSearchText(String((textField.text ?? "").dropLast()))
            if searchField.text?.count == 1{
                searchField.text = ""
                filteredPerson.removeAll()
                setAnnotations()
                tableView.reloadData()
            }
        }
       
        print(searchField.text ?? "")
        return true
    }
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return searchField.text?.isEmpty ?? true
    }
    
    func filterContentForSearchText(_ searchText: String, scope: String = "All") {
        filteredPerson = PersonData.filter({( person : PersonInfo) -> Bool in
            return person.name.lowercased().contains(searchText.lowercased())
        })
        
        setAnnotations()
        tableView.reloadData()
    }
    
    private func setAnnotations(){
        var annotation : MKPointAnnotation!
        mapView.removeAnnotations(mapView.annotations)
        if isFiltering(){
            for filters in filteredPerson{
                annotation = MKPointAnnotation()
                self.location = filters.latlng
                annotation.coordinate = self.location
                annotation.title = filters.name
                annotation.subtitle = String(filters.id)
                self.mapView.addAnnotation(annotation)
            }
        }else{
            for filters in PersonData{
                annotation = MKPointAnnotation()
                self.location = filters.latlng
                annotation.coordinate = self.location
                annotation.title = filters.name
                annotation.subtitle = String(filters.id)
                self.mapView.addAnnotation(annotation)
            }
        }
    }
    
    func isFiltering() -> Bool {
        return  !(searchField.text?.isEmpty)!
    }
    
    private func getTheapistsData(){
        MoyaNetworkCallGenericFunction(TherapistService.GetTherapistNames,
                                       { (error) in
                                     //   UIApplication.genericAlertWithCompletion(title: "Error", descr: error.localizedDescription)
                                        
        }) {[weak self] (response) in
            guard let weak = self else {return}
            do{
                let json = try JSONDecoder().decode(TherapistListingResult.self, from: response.data)
                if let result = json{
                    for data in result{
                        weak.PersonData.append(PersonInfo(name: "\(data.firstName ?? "") \(data.lastName ?? "")", expert: data.signature ?? "", Photo: data.picture ?? "",id: data.userID ?? 0,latlng: CLLocationCoordinate2D(latitude: data.latitude ?? 0, longitude: data.longitude ?? 0)))
                    }
                        weak.filteredPerson = weak.PersonData
                        weak.refresh.endRefreshing()
                    DispatchQueue.main.async {
                        weak.tableView.reloadData()
                    }
                    //    weak.view.stopSkeletonAnimation()
                   //     weak.reloadEmptyStateForTableView(weak.tableView)
                 //   } else{
                     //   weak.refresh.endRefreshing()
                       // weak.reloadEmptyStateForTableView(weak.tableView)
                        //  UIApplication.genericAlertWithCompletion(title: "Error", descr: json.message ?? "Unable to fetch store, try again!.")
                        // weak.navigationController?.popViewController(animated: true)
                 //   }
                }else{
                    weak.refresh.endRefreshing()
                  //  weak.reloadEmptyStateForTableView(weak.tableView)
                 //   UIApplication.genericAlertWithCompletion(title: "Error", descr: json.message ?? "")
                    weak.navigationController?.popViewController(animated: true)
                }
            }
            catch{
                 weak.refresh.endRefreshing()
                //  UIApplication.genericAlertWithCompletion(title: "Error", descr: error.localizedDescription)
            }
        }
    }
    
    private func getTheapistsLocation(){
        MoyaNetworkCallGenericFunction(TherapistService.GetTherapistLocation,
                                       { (error) in
                                        //   UIApplication.genericAlertWithCompletion(title: "Error", descr: error.localizedDescription)
                                        
        }) {[weak self] (response) in
            guard let weak = self else {return}
            do{
                let json = try JSONDecoder().decode(TherapistLocationResult.self, from: response.data)
                if let result = json{
                    var annotation : MKPointAnnotation!
                    for data in result{
                        weak.location = CLLocationCoordinate2D(latitude: data.latitude ?? 0, longitude: data.longitude ?? 0)
                        annotation = MKPointAnnotation()
                        annotation.coordinate = weak.location
                        annotation.title = "\(data.firstName ?? "") \(data.lastName ?? "")"
                        annotation.subtitle = String(data.userID ?? 0)
                       // annotation.accessibilityHint =
                        weak.mapView.addAnnotation(annotation)
                    }
                }else{
                    //  weak.reloadEmptyStateForTableView(weak.tableView)
                    //   UIApplication.genericAlertWithCompletion(title: "Error", descr: json.message ?? "")
                    weak.navigationController?.popViewController(animated: true)
                }
            }
            catch{
              //  weak.refresh.endRefreshing()
                //  UIApplication.genericAlertWithCompletion(title: "Error", descr: error.localizedDescription)
            }
        }
    }
}
