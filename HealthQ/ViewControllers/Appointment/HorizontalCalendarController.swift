//
//  HorizontalCalendarController.swift
//  VACalendarExample
//
//  Created by Anton Vodolazkyi on 09.03.18.
//  Copyright © 2018 Anton Vodolazkyi. All rights reserved.
//

import UIKit
import VACalendar
import TTGSnackbar
final class HorizontalCalendarController: BaseController {
    
    @IBOutlet weak var cardView:UIView!
    @IBOutlet weak var bookBtn:UIButton!
    @IBOutlet weak var loading:UIActivityIndicatorView!
    var therapistID = Int()
    var clinicID = Int()
    var date : String = ""
    var weekName = [String]()
    @IBOutlet weak var monthHeaderView: VAMonthHeaderView! {
        didSet {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy LLLL"
            
            let appereance = VAMonthHeaderViewAppearance(
                monthFont: UIFont.systemFont(ofSize: 16),
                monthTextColor: #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0),
                monthTextWidth: 200,
                previousButtonImage: #imageLiteral(resourceName: "back"),
                nextButtonImage: #imageLiteral(resourceName: "forward"),
                dateFormatter: dateFormatter
            )
           // monthHeaderView.setViewCard()
            monthHeaderView.layer.cornerRadius = 20
            monthHeaderView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
            monthHeaderView.delegate = self
            monthHeaderView.appearance = appereance
            monthHeaderView.layer.backgroundColor = #colorLiteral(red: 0, green: 0.4470588235, blue: 0.7764705882, alpha: 1)
            monthHeaderView.roundCorners([.topLeft, .topRight], radius: 4)
            let path = UIBezierPath(rect: monthHeaderView.bounds)
            let border = CAShapeLayer()
            border.path = path.cgPath
            border.lineWidth = 0.5
            border.fillColor = UIColor.clear.cgColor
            border.strokeColor = UIColor.lightGray.cgColor
            monthHeaderView.layer.addSublayer(border)
        }
    }
    
    @IBOutlet weak var weekDaysView: VAWeekDaysView! {
        didSet {
            let appereance = VAWeekDaysViewAppearance(symbolsType: .veryShort, calendar: defaultCalendar)
            weekDaysView.appearance = appereance
          //  weekDaysView.setViewCard()
        }
    }
    
    let defaultCalendar: Calendar = {
        var calendar = Calendar.current
        calendar.firstWeekday = 1
        calendar.timeZone = TimeZone(secondsFromGMT: 0)!
        return calendar
    }()
    
    var calendarView: VACalendarView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        self.navigationItem.title = "Select Timeslot"
        bookBtn.roundView(with: 5)
        let calendar = VACalendar(calendar: defaultCalendar)
        calendarView = VACalendarView(frame: .zero, calendar: calendar)
        calendarView.layer.cornerRadius = 20
        calendarView.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
//        calendarView = VACalendarView(frame: CGRect(
//            x: 0, y: weekDaysView.frame.maxY,
//            width: view.frame.width,
//            height: view.frame.height - weekDaysView.frame.maxY
//        ), calendar: calendar)
        cardView.setViewCard()
        calendarView.layer.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        calendarView.showDaysOut = true
        calendarView.selectionStyle = .single
        calendarView.monthDelegate = monthHeaderView
        calendarView.dayViewAppearanceDelegate = self
        calendarView.monthViewAppearanceDelegate = self
        calendarView.calendarDelegate = self
        calendarView.scrollDirection = .horizontal
        
      //  let isoDate = "2019-04-15"
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
     //   let date = dateFormatter.date(from:isoDate)!
     //   let timeInterval = date.timeIntervalSinceNow
        calendarView.setSupplementaries([
         //   (Date().addingTimeInterval(timeInterval), [VADaySupplementary.bottomDots([.red])]),
         //   (Date().addingTimeInterval((60 * 60 * 110)), [VADaySupplementary.bottomDots([.red])]),
        //    (Date().addingTimeInterval((60 * 60 * 370)), [VADaySupplementary.bottomDots([.red])]),
        //    (Date().addingTimeInterval((60 * 60 * 430)), [VADaySupplementary.bottomDots([.red])])
          ])
        view.addSubview(calendarView)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if calendarView.frame == .zero {
            calendarView.frame = CGRect(
                x: 15,
                y: 160,
                width: view.frame.width * 0.92,
                height: view.frame.height * 0.4
            )
            calendarView.setup()
        }
    }
    
    @IBAction func book(sender:UIButton){
        if date != ""{
        if (defaults.object(forKey: "mrn") as? String ?? "") != ""{
            loading.startAnimating()
            self.view.isUserInteractionEnabled = false
            self.bookBtn.showSkeleton(usingColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            MoyaNetworkCallGenericFunction(AuthenticateService.SignUp(PATIENT_ID: "", PATIENT_MRN: mrn, PATIENT_FNAME: "", PATIENT_MNAME: "", PATIENT_LNAME: "", PATIENT_EMAIL: "", APPOINTMENT_DATE: date, CLINIC_ID: clinicID, THERAPIST_ID: therapistID, PATIENT_PASSWORD: "") ,
                                           { (error) in
                                            self.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                                            
            }) {[weak self] (response) in
                guard let weak = self else {return}
                do{
                    let json = try JSONDecoder().decode(AppointmentResult.self, from: response.data)
                    if  json.response == "SUCCESS"{
                        // print(SignInResult.self)
                        //   weak.defaults.set("", forKey: "mrn")
                        weak.ShowResults(Message: "Appointment booked successfully", color: #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 0.5))
                        weak.StopAllControl()
                        let alert = UIAlertController(title: "Appointment booked successfully at", message: "\(json.appointmentDatetime ?? weak.date)", preferredStyle: .alert)
                        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { (_) in
                            self?.navigationController?.popViewController(animated: true)
                        }))
                        weak.present(alert, animated: true, completion: nil)
                    }else{
                        weak.ShowResults(Message: "Appointment Failed", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        weak.StopAllControl()
                    }
                }
                catch{
                    weak.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.521568656, green: 0.1098039225, blue: 0.05098039284, alpha: 1))
                    weak.StopAllControl()
                }
                
            }
        }else{
            let snackbar = TTGSnackbar(message: "Login Required..!", duration: .long)
            // Action 1
            snackbar.actionText = "Signin"
            snackbar.actionTextColor = UIColor.green
            snackbar.actionBlock = { (snackbar) in
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInController") as? SignInController{
                    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
                    self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            
            // Action 2
            snackbar.secondActionText = "Signup"
            snackbar.secondActionTextColor = UIColor.yellow
            snackbar.secondActionBlock = { (snackbar) in
                if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpController") as? SignUpController{
                    vc.clinicID = self.clinicID
                    vc.therapistID = self.therapistID
                    vc.date = self.date
                    self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
                    self.navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            }
            snackbar.show()
         }
      }else{
            ShowResults(Message: "Select from give working day", color: #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))
        }
    }
    
    func StopAllControl(){
        loading.stopAnimating()
        bookBtn.hideSkeleton()
        view.isUserInteractionEnabled = true
    }
}

extension HorizontalCalendarController: VAMonthHeaderViewDelegate {
    
    func didTapNextMonth() {
        calendarView.nextMonth()
    }
    
    func didTapPreviousMonth() {
        calendarView.previousMonth()
    }
    
}

extension HorizontalCalendarController: VAMonthViewAppearanceDelegate {
    
    func leftInset() -> CGFloat {
        return 10.0
    }
    
    func rightInset() -> CGFloat {
        return 10.0
    }
    
    func verticalMonthTitleFont() -> UIFont {
        return UIFont.systemFont(ofSize: 16, weight: .semibold)
    }
    
    func verticalMonthTitleColor() -> UIColor {
        return .black
    }
    
    func verticalCurrentMonthTitleColor() -> UIColor {
        return .red
    }
    
}

extension HorizontalCalendarController: VADayViewAppearanceDelegate {
    
    func textColor(for state: VADayState) -> UIColor {
        switch state {
        case .out:
            return UIColor(red: 214 / 255, green: 214 / 255, blue: 219 / 255, alpha: 1.0)
        case .selected:
            return .white
        case .unavailable:
            return .lightGray
        default:
            return .black
        }
    }
    
    func textBackgroundColor(for state: VADayState) -> UIColor {
        switch state {
        case .selected:
            return .red
        default:
            return .clear
        }
    }
    
    func shape() -> VADayShape {
        return .circle
    }
    
    func dotBottomVerticalOffset(for state: VADayState) -> CGFloat {
        switch state {
        case .selected:
            return 2
        default:
            return -7
        }
    }
    
}

extension HorizontalCalendarController: VACalendarViewDelegate {
    
    func selectedDates(_ dates: [Date]) {
        let formatter = DateFormatter()
        formatter.dateFormat  = "EEE"
        let dayofweek =  formatter.string(from: dates.last ?? Date())
        print(dayofweek)
        print(weekName)
        if weekName.contains(dayofweek){
            calendarView.startDate = dates.last ?? Date()
            formatter.dateStyle = .medium
            formatter.dateFormat = "YYYY-MM-dd"
            date = (formatter.string(from: dates.last ?? Date()))
        }else{
            date = ""
           // ShowResults(Message: "Select the working day", color: #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1))
        }
    }
    
}

