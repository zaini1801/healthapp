//
//  BaseController.swift
//  HealthQ
//
//  Created by Zaini on 04/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit
import TTGSnackbar
import Network

@available(iOS 12.0, *)
class BaseController: UIViewController {

    let defaults = UserDefaults.standard
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var mrn : String = ""
    var email : String = ""
    let monitor = NWPathMonitor()
   // let cellMonitor = NWPathMonitor(requiredInterfaceType: .cellular)
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        DispatchQueue.main.async {
            self.monitor.pathUpdateHandler = { path in
                if path.status == .satisfied {
                    print("We're connected!")
                    //self.showInternet(message: "Internet Availble", duration: .short)
                    self.hideInternet()
                } else {
                    print("No connection.")
                    self.showInternet(message: "Internet Connection lost", duration: .forever)
                }
            }
            let queue = DispatchQueue(label: "Monitor")
            self.monitor.start(queue: queue)
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        mrn = defaults.object(forKey: "mrn") as? String ?? ""
        email = defaults.object(forKey: "email") as? String ?? ""
    }
    var internetSnack = TTGSnackbar()
    func showInternet(message:String,duration:TTGSnackbarDuration){
        DispatchQueue.main.async {
            self.internetSnack = TTGSnackbar(message: message, duration: duration)
            self.internetSnack.messageTextColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.internetSnack.backgroundColor = #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1)
            self.internetSnack.show()
        }
    }
    
    func hideInternet(){
        internetSnack.dismiss()
    }
    
    func ShowResults(Message:String,color:UIColor){
        let snackbar = TTGSnackbar(message: Message, duration: .middle)
         snackbar.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        snackbar.backgroundColor = #colorLiteral(red: 0.06666666667, green: 0.7529411765, blue: 0.9529411765, alpha: 1)
        snackbar.show()
    }
    
    func WaitResults(Message:String){
       let snackbar =  TTGSnackbar(message: Message, duration: .middle)
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(3 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)) {
            snackbar.dismiss()
        }
        snackbar.show()
    }
}
