//
//  SignUpController.swift
//  HealthQ
//
//  Created by Zaini on 01/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
class SignUpController: BaseController {
    
    @IBOutlet weak var firstLbl:UITextField!
    @IBOutlet weak var lastLbl:UITextField!
    @IBOutlet weak var emailLbl:UITextField!
    @IBOutlet weak var passwordLbl:UITextField!
    @IBOutlet weak var cardView:UIView!
    @IBOutlet weak var signBtn:UIButton!
    @IBOutlet weak var loading:UIActivityIndicatorView!
    var therapistID : Int = 0
    var clinicID : Int = 0
    var date = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Signup"
        signBtn.roundView(with: 5)
        cardView.setViewCard()
    }
    
    @IBAction func SignUpAction(sender:UIButton){
        if (firstLbl.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            ShowResults(Message: "Enter your first name", color: .clear)
        }else if (lastLbl.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            ShowResults(Message: "Enter your last name", color: .clear)
        }else if (emailLbl.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            ShowResults(Message: "Enter your email", color: .clear)
        }else if (passwordLbl.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            ShowResults(Message: "Enter your password", color: .clear)
        }else{
            loading.startAnimating()
            self.view.isUserInteractionEnabled = false
            self.signBtn.showSkeleton(usingColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            MoyaNetworkCallGenericFunction(AuthenticateService.SignUp(PATIENT_ID: "", PATIENT_MRN: "", PATIENT_FNAME: firstLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", PATIENT_MNAME: "", PATIENT_LNAME: lastLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", PATIENT_EMAIL: emailLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", APPOINTMENT_DATE: date, CLINIC_ID: clinicID, THERAPIST_ID: therapistID, PATIENT_PASSWORD: passwordLbl.text?.trimmingCharacters(in: .whitespaces) ?? "") ,
                                           { (error) in
                                            self.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                                            
            }) {[weak self] (response) in
                guard let weak = self else {return}
                do{
                    let json = try JSONDecoder().decode(AppointmentResult.self, from: response.data)
                    if  json.response == "SUCCESS"{
                        // print(SignInResult.self)
                        weak.mrn = json.patientMrn ?? ""
                        weak.email = weak.emailLbl.text?.trimmingCharacters(in: .whitespaces) ?? ""
                        weak.defaults.set(json.patientMrn ?? "", forKey: "mrn")
                        weak.defaults.set(weak.firstLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", forKey: "fname")
                        weak.defaults.set(weak.lastLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", forKey: "lname")
                        weak.defaults.set(weak.emailLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", forKey: "email")
                        weak.ShowResults(Message: "Successfully Signed Up", color: #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 0.5))
                        weak.StopAllControl()
                        weak.navigationController?.popViewController(animated: true)
                    }else{
                        weak.ShowResults(Message: "Logged In Failed", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        weak.StopAllControl()
                    }
                }
                catch{
                    weak.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                    weak.StopAllControl()
                }
                
            }
        }
    }
    
    func StopAllControl(){
        loading.stopAnimating()
        signBtn.hideSkeleton()
        view.isUserInteractionEnabled = true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
