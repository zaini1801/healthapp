//
//  SignInController.swift
//  HealthQ
//
//  Created by Zaini on 04/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
class SignInController: BaseController {

     @IBOutlet weak var cardView:UIView!
    @IBOutlet weak var emailLbl:UITextField!
    @IBOutlet weak var passwordLbl:UITextField!
    @IBOutlet weak var signBtn:UIButton!
    @IBOutlet weak var loading:UIActivityIndicatorView!
    override func viewDidLoad() {
        super.viewDidLoad()
        cardView.setViewCard()
        self.navigationItem.title = "Signin"
    }
    
    @IBAction func SignInAction(sender:UIButton){
        if (emailLbl.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            ShowResults(Message: "Enter your email", color: .clear)
        }else if (passwordLbl.text?.trimmingCharacters(in: .whitespaces).isEmpty)!{
            ShowResults(Message: "Enter your password", color: .clear)
        }else{
            loading.startAnimating()
            self.view.isUserInteractionEnabled = false
            self.signBtn.showSkeleton(usingColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
            MoyaNetworkCallGenericFunction(AuthenticateService.SignIn(patientEmail: emailLbl.text?.trimmingCharacters(in: .whitespaces) ?? "", password: passwordLbl.text?.trimmingCharacters(in: .whitespaces) ?? ""),
                                           { (error) in
                                            self.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                                            
            }) {[weak self] (response) in
                guard let weak = self else {return}
                do{
                   let json = try JSONDecoder().decode(LoginResult.self, from: response.data)
                    if let data = json.patientMrn{
                        weak.defaults.synchronize()
                        weak.defaults.set(data, forKey: "mrn")
                        weak.ShowResults(Message: "Successfully Logged In", color: #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 0.5))
                        weak.StopAllControl()
                        weak.navigationController?.popViewController(animated: true)
                    }else{
                        weak.ShowResults(Message: "Logged In Failed", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                        weak.StopAllControl()
                    }
                }
                catch{
                    weak.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                    weak.StopAllControl()
                }
                
            }
        }
    }
    
    func StopAllControl(){
        loading.stopAnimating()
        signBtn.hideSkeleton()
        view.isUserInteractionEnabled = true
    }
    
//    UserDefaults.standard.synchronize()
//    UserDefaults.standard.removeObject(forKey: "mrn")

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
