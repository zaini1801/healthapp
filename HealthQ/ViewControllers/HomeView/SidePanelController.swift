//
//  SidePanelController.swift
//  HealthQ
//
//  Created by Zaini on 30/04/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit

class SidePanelController: UIViewController,UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var tableView: UITableView!
    
    fileprivate let menuOptions = ["Home", "Music", "Contacts", "Videos", "Apple", "Messages", "Cloud", "Support"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewConfigurations()
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    private func viewConfigurations() {
        
       // tableView.register(UINib.init(nibName: "LeftMenuCell", bundle: nil), forCellReuseIdentifier: "LeftMenuCell")
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuOptions.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell")
            else {return UITableViewCell()}
            cell.textLabel?.text = menuOptions[indexPath.row]
            return cell
      //  cell.menuOption.text = menuOptions[indexPath.row]
       // cell.menuImage.image = UIImage(named: "right_menu_" + String(indexPath.row + 1))
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        
        let mainStoryboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        var identifier = ""
        
        if indexPath.row % 2 == 0 {
            identifier = "CenterVC1"
        }
        else{
            identifier = "CenterVC2"
        }
        
        let centerVC: UIViewController = mainStoryboard.instantiateViewController(withIdentifier: identifier)
        let centerNavVC = UINavigationController(rootViewController: centerVC)
        
        panel!.configs.bounceOnCenterPanelChange = true
        
        /*
         // Simple way of changing center PanelVC
         _ = panel!.center(centerNavVC)
         */
        
        
        
        /*
         New Feature Added, You can change the center panelVC and after completion of the animations you can execute a closure
         */
        
        panel!.center(centerNavVC, afterThat: {
            print("Executing block after changing center panelVC From Left Menu")
            //            _ = self.panel!.left(nil)
        })
    }

}
