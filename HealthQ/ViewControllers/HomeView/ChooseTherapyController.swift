//
//  ChooseTherapyController.swift
//  HealthQ
//
//  Created by Zaini on 30/04/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import UIKit

@available(iOS 12.0, *)
class ChooseTherapyController: BaseController {

    @IBOutlet weak var cardView:UIView!
    @IBOutlet weak var profilePhoto:UIView!
    @IBOutlet weak var profilePhoto1:UIView!
    @IBOutlet weak var FullName:UILabel!
    @IBOutlet weak var totalTherapist:UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
      //  panel!.delegate = self
       
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Avenir-Medium", size: 18)!]
        cardView.roundView(with: 6, UIColor.clear, 1, true)
        profilePhoto.setViewCard()
        profilePhoto1.setViewCard()
        totalTherapist.showAnimatedSkeleton()
        getTotalTherapist()
    }
    
    override func viewDidAppear(_ animated: Bool) {
         FullName.text = (defaults.object(forKey: "fname") as? String ?? "") + " " + (defaults.object(forKey: "lname") as? String ?? "")
    }
    @IBAction func SidePanel(sender:UIBarButtonItem){
     //   panel?.openLeft(animated: true)
    }
    
    @IBAction func discoverBtn(sender:UIButton){
        if let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "DiscoverTherapyController") as? DiscoverTherapyController{
            navigationItem.backBarButtonItem = UIBarButtonItem(title: "Back", style: .plain, target: nil, action: nil)
            navigationItem.backBarButtonItem?.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onlineBtn(sender:UIButton){
        
    }
    
    private func getTotalTherapist(){
        MoyaNetworkCallGenericFunction(AuthenticateService.GetTherapistCount,
                                       { (error) in
                                        self.ShowResults(Message: error.localizedDescription, color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                                        
        }) {[weak self] (response) in
            guard let weak = self else {return}
            do{
                let json = try JSONDecoder().decode(GetTherapistResult.self, from: response.data)
                weak.totalTherapist.text = String(json.totalTherapist ?? 0) + " therapists"
                weak.totalTherapist.hideSkeleton()
            }
            catch{
                weak.ShowResults(Message: "no therapists found", color: #colorLiteral(red: 0.7450980544, green: 0.1568627506, blue: 0.07450980693, alpha: 1))
                weak.totalTherapist.text = "0 therapists"
                weak.totalTherapist.hideSkeleton()
            }
        }
    }
}

//extension ChooseTherapyController: FAPanelStateDelegate {
//
//    func centerPanelWillBecomeActive() {
//        print("centerPanelWillBecomeActive called")
//    }
//
//    func centerPanelDidBecomeActive() {
//        print("centerPanelDidBecomeActive called")
//    }
//
//
//    func leftPanelWillBecomeActive() {
//        print("leftPanelWillBecomeActive called")
//    }
//
//
//    func leftPanelDidBecomeActive() {
//        print("leftPanelDidBecomeActive called")
//    }
//
//
//    func rightPanelWillBecomeActive() {
//        print("rightPanelWillBecomeActive called")
//    }
//
//    func rightPanelDidBecomeActive() {
//        print("rightPanelDidBecomeActive called")
//    }
//}
