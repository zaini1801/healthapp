//
//  TherapistService.swift
//  HealthQ
//
//  Created by Zaini on 30/04/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import Foundation

let Api_URL = URL(string: "http://124.29.202.246/TherapieHealth/api/General")
let Image_URL = "http://124.29.202.246/TherapieHealth/"
import Moya

enum TherapistService{
    case GetTherapistNames
    case GetTherapistByNames(name:String)
    case GetTherapistLocation
    case GetTherapistById(Id:Int)
    case GetTherapistScheduleById(id:Int)
}

extension TherapistService:TargetType,AccessTokenAuthorizable{
    public var baseURL: URL {
        return Api_URL!
    }
    public var path: String {
        switch self {
        case .GetTherapistNames:
            return "/GetTherapistNames"
        case .GetTherapistByNames:
            return "/GetTherapistByNames"
        case .GetTherapistLocation:
            return "/GetTherapistLocation"
        case .GetTherapistById:
            return "/GetTherapistById"
        case .GetTherapistScheduleById:
            return "/GetTherapistScheduleById"
        }
    }
    var authorizationType: AuthorizationType{
        return .none
    }
    
    public var method: Moya.Method {
        switch self {
        default :
            return .get
        }
    }
    public var sampleData: Data {
        return Data()
    }
    public var task: Task{
        switch self {
        case .GetTherapistNames:
           return .requestPlain
        case .GetTherapistByNames(let name):
            return .uploadMultipart([MultipartFormData(provider: .data(name.data(using: .utf8) ?? Data()), name: "name")])
        case .GetTherapistLocation:
            return .requestPlain
        case .GetTherapistById(let Id):
           return .requestParameters(parameters: ["Id":Id], encoding: URLEncoding.queryString)
        case .GetTherapistScheduleById(let id):
            return .uploadMultipart([MultipartFormData(provider: .data("\(id)".data(using: .utf8) ?? Data()), name: "id")])
        }
    }
    public var headers: [String : String]? {
        return ["Content-Type":"application/json; charset=utf-8"]
    }
    
}
