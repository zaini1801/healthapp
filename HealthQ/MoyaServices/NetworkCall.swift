//
//  NetworkCall.swift
//  HealthQ
//
//  Created by Zaini on 02/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//
import Foundation
import Moya
import Alamofire

//For Network Call
func MoyaNetworkCallGenericFunction<T: TargetType>(_ target: T, _ error:@escaping (MoyaError)->Void,_ result:@escaping (Response)->Void) {
 
    let provider = MoyaProvider<T>(manager:CustomWebService.manager(),plugins: [NetworkLoggerPlugin(verbose: true, responseDataFormatter: JSONResponseDataFormatter)])
    print("Parameters OF API ===========> Starting")
    dump(target)
    print("Parameters OF API ===========> Ending")
    provider.request(target) { (Result) in
        switch Result {
        case .failure(let err):
            error(err)
        case .success(let res):
            result(res)
        }
    }
    
}
//For shared Message Status Decoder
//func decodeSharedResponse(_ data : Response) -> SharedResultModel{
//
//    do{
//        let json = try JSONDecoder().decode(SharedResultModel.self, from: data.response)
//
//    }
//    catch{
//
//    }
//}

//JsonData Formatting
private func JSONResponseDataFormatter(_ data: Data) -> Data{
    
    do{
        let dataAsJson = try JSONSerialization.jsonObject(with: data)
        let prettyData = try JSONSerialization.data(withJSONObject: dataAsJson, options: .prettyPrinted)
        return prettyData
    }
    catch
    {
        return data
        
    }
}

//Custom Webservice

class CustomWebService {
    // session manager
    static func manager() -> Alamofire.SessionManager {
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = 20 // timeout
        let manager = Alamofire.SessionManager(configuration: configuration)
        manager.adapter = CustomRequestAdapter()
        return manager
    }
    
    // request adpater to add default http header parameter
    private class CustomRequestAdapter: RequestAdapter {
        public func adapt(_ urlRequest: URLRequest) throws -> URLRequest {
            var urlRequest = urlRequest
            urlRequest.setValue("XMLHttpRequest", forHTTPHeaderField: "X-Requested-With")
            return urlRequest
        }
    }
}
