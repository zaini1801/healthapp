//
//  AuthenticateService.swift
//  HealthQ
//
//  Created by Zaini on 04/05/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//
import Foundation
import Moya

enum AuthenticateService{
    case SignUp(PATIENT_ID:String,PATIENT_MRN:String,PATIENT_FNAME:String,PATIENT_MNAME:String, PATIENT_LNAME:String,PATIENT_EMAIL:String,APPOINTMENT_DATE:String,CLINIC_ID:Int,THERAPIST_ID:Int,PATIENT_PASSWORD:String)
    case SignIn(patientEmail:String,password:String)
    case GetTherapistCount
    //alex.carlton@outlook.com
    //abcd1234
}

extension AuthenticateService:TargetType,AccessTokenAuthorizable{
    public var baseURL: URL {
        return Api_URL!
    }
    public var path: String {
        switch self {
        case .SignUp:
            return "/BookAppointment"
        case .SignIn:
            return "/ValidatePatient"
        case .GetTherapistCount:
            return "/GetTherapistCount"
        }
    }
    var authorizationType: AuthorizationType{
        return .none
    }
    
    public var method: Moya.Method {
        switch self {
        case .GetTherapistCount:
            return .get
        default :
            return .post
        }
    }
    public var sampleData: Data {
        return Data()
    }
    public var task: Task{
        switch self {
        case .SignUp(let PATIENT_ID,let PATIENT_MRN,let PATIENT_FNAME,let PATIENT_MNAME, let PATIENT_LNAME,let PATIENT_EMAIL,let APPOINTMENT_DATE,let CLINIC_ID,let THERAPIST_ID,let PATIENT_PASSWORD):

            return .requestParameters(parameters: ["PATIENT_ID": PATIENT_ID,"PATIENT_MRN": PATIENT_MRN,"PATIENT_FNAME": PATIENT_FNAME,"PATIENT_MNAME": PATIENT_MNAME,"PATIENT_LNAME": PATIENT_LNAME,"PATIENT_EMAIL": PATIENT_EMAIL,"APPOINTMENT_DATE": APPOINTMENT_DATE,"CLINIC_ID": CLINIC_ID,"THERAPIST_ID": THERAPIST_ID,"PATIENT_PASSWORD":PATIENT_PASSWORD], encoding: JSONEncoding.default)
            //  return .uploadMultipart([MultipartFormData(provider: .data(PATIENT_ID.data(using: .utf8) ?? Data()), name: "PATIENT_ID"),MultipartFormData(provider: .data(PATIENT_MRN.data(using: .utf8) ?? Data()), name: "PATIENT_MRN"),MultipartFormData(provider: .data(PATIENT_FNAME.data(using: .utf8) ?? Data()), name: "PATIENT_FNAME"),MultipartFormData(provider: .data(PATIENT_MNAME.data(using: .utf8) ?? Data()), name: "PATIENT_MNAME"),MultipartFormData(provider: .data(PATIENT_LNAME.data(using: .utf8) ?? Data()), name: "PATIENT_LNAME"),MultipartFormData(provider: .data(PATIENT_EMAIL.data(using: .utf8) ?? Data()), name: "APPOINTMENT_DATE"),MultipartFormData(provider: .data(APPOINTMENT_DATE.data(using: .utf8) ?? Data()), name: "APPOINTMENT_DATE"),MultipartFormData(provider: .data(PATIENT_EMAIL.data(using: .utf8) ?? Data()), name: "PATIENT_EMAIL"),MultipartFormData(provider: .data("\(CLINIC_ID)".data(using: .utf8) ?? Data()), name: "CLINIC_ID"),MultipartFormData(provider: .data("\(THERAPIST_ID)".data(using: .utf8) ?? Data()), name: "THERAPIST_ID"),MultipartFormData(provider: .data(PATIENT_PASSWORD.data(using: .utf8) ?? Data()), name: "PATIENT_PASSWORD")])
            case .SignIn(let patientEmail,let password):
               return .requestParameters(parameters: ["PATIENT_EMAIL": patientEmail,"PATIENT_PASSWORD": password], encoding: JSONEncoding.default)
        case .GetTherapistCount:
            return .requestPlain
        }
    }
    public var headers: [String : String]? {
        return ["Content-Type":"application/json; charset=utf-8"]
    }
    
}
