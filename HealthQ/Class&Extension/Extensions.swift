//
//  Extensions.swift
//  HealthQ
//
//  Created by Zaini on 30/04/2019.
//  Copyright © 2019 Zaini. All rights reserved.
//

import Foundation
import UIKit

extension UIView{
    func setViewCard(_ cornerRadius: CGFloat = 10, _ shadowColor: CGColor = UIColor.lightGray.cgColor) {
        self.layer.masksToBounds = false
        self.layer.shadowColor = shadowColor
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.layer.shadowOpacity = 0.8
        self.layer.cornerRadius = cornerRadius
    }
    func removeCardView() {
        self.layer.masksToBounds = false
        self.layer.shadowOpacity = 0.0
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowColor = UIColor.clear.cgColor
    }
    
    func roundView(with radius:CGFloat,_ borderColor: UIColor = .clear, _ borderSize: CGFloat = 1, _ cardView: Bool = false){
        if cardView{
            setViewCard(radius)
        }else{
            
            self.layer.cornerRadius = radius
        }
        self.layer.borderColor = borderColor.cgColor
        self.layer.borderWidth = borderSize
        self.clipsToBounds = true
        
    }
    
    func roundCorners(_ corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: [.usesLineFragmentOrigin, .usesFontLeading], attributes: [NSAttributedString.Key.font: font], context: nil)
        return boundingBox.height
    }
}
